package edu.umb.cs443.finals;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;

public class Tower {
    private String towerType;
    private int position;
    private int level;
    private int damage;
    private int cost;
    private float range;
    private static int posx,posy;

    public Tower(String towerType, int position) {
        if (towerType.equals("X_1")) {
            setBasicTower();
        } else if (towerType.equals("X_2")) {
            setBasicTower();
            incrLevel();
        } else if (towerType.equals("X_3")) {
            setBasicTower();
            incrLevel();
            incrLevel();
        }
        this.position = position;
        this.towerType = towerType;
    }

    // Getters & Setters
    public int getLevel() {return this.level;}
    // Where the stats of each attribute is allocated
    private void incrLevel() {
        this.level += 1;
        this.cost *= 2;
        this.damage *= 3;
        this.range += 80f;
    }
    public int getDamage() {return this.damage;}
    public int getCost() {return this.cost;}
    public int getPositionX() {return this.posx;}
    public void setPositionX(int newpos) {this.posx=newpos;}
    public int getPositionY() {return this.posy;}
    public void setPositionY(int newpos) {this.posy=newpos;}
    public int getPosition() {return this.position;}
    public String getTowerType() {return this.towerType;}
    public float getRange() {return this.range;}

    private void setBasicTower() {
        this.level = 1;
        this.damage = 5;
        this.cost = 20;
        this.range = 250f;
    }
}
