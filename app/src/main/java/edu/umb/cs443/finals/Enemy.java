package edu.umb.cs443.finals;

import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class Enemy {
    private String name;
    private int difficulty;
    private int life, max_life;
    private int reward;
    private int posx,posy;
    private String[] map;
    private static int X_HP_BAR_OFFSET = 40;
    private static int Y_HP_BAR_OFFSET = 5;
    private int velocity = 400;
    private int traversed;
    private ImageView enemyImageView;
    private ProgressBar enemyHPBar;
    private boolean prepareKill;
    private static int thres = 40;
    private float attackRange = 100;

    public Enemy(int posx, int posy, int difficulty, String[] map) {
        this.posx = posx;
        this.posy = posy;
        this.difficulty = difficulty;
        this.max_life = 7 + this.difficulty * 17;
        this.life = (int) this.max_life;
        this.reward = 1 + (int) Math.floor(this.difficulty / 3);
        this.map = map;
        this.traversed = 0;
        this.prepareKill = false;
    }

    // Getters and Setters
    public String getName() {return this.name;}
    public int getDifficulty() {return this.difficulty;}
    public int getVelocity() {return this.velocity;}
    public void setVelocity(int velocity) {this.velocity = velocity;}
    public int getLife() {return this.life;}
    public int getMax_life() {return this.max_life;}
    public int getReward() {return this.reward;}
    public int getPosX() {return this.posx;}
    public void setPosX(int posx) {this.posx = posx;}
    public int getPosY() {return this.posy;}
    public void setPosY(int posy) {this.posy = posy;}
    public ImageView getEnemyImageView() {return this.enemyImageView;}
    public ProgressBar getEnemyHPBar() {return this.enemyHPBar;}
    public int getTraversed() {return this.traversed;}
    public boolean getPrepareKill() {return this.prepareKill;}
    public void setPrepareKill() {this.prepareKill = true;}

    @SuppressLint("ResourceType")
    public void setEnemyImageView(ImageView enemyImageView) {
        // Set enemy Sprite
        this.enemyImageView = enemyImageView;
        enemyImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        enemyImageView.setLayoutParams(new GridView.LayoutParams(170, 170));
        enemyImageView.setBackgroundResource(R.anim.zombie_animate);
        AnimationDrawable enemyAnimation = (AnimationDrawable) enemyImageView.getBackground();
        enemyAnimation.start();
        enemyImageView.setX((float) getPosX());
        enemyImageView.setY((float) getPosY());
        // Set enemy HP bar
    }

    public void setEnemyHPBar(ProgressBar enemyHPBar) {
        this.enemyHPBar = enemyHPBar;
        enemyHPBar.setX((float) getPosX() + X_HP_BAR_OFFSET);
        enemyHPBar.setY((float) getPosY() + Y_HP_BAR_OFFSET);
        enemyHPBar.setLayoutParams(new GridView.LayoutParams(90, 20));
        enemyHPBar.setMax(getMax_life());
        enemyHPBar.setProgress(getLife());
    }

    public void hit(int damage) {
        this.life -= damage;
        enemyHPBar.setProgress(getLife());
    }

    public void reachedEnd() {
        this.prepareKill = true;
    }

    public void move(int destX, int destY) {
        int deltaX = destX - posx;
        int deltaY = destY - posy;
        this.posx = (deltaX > 0) ? this.posx + this.velocity : this.posx - this.velocity;
        this.posy = (deltaY > 0) ? this.posy + this.velocity : this.posy - this.velocity;
        // Move sprite and hp bar
        enemyImageView.setX((float) this.posx);
        enemyImageView.setY((float) this.posy);
        enemyHPBar.setX((float) this.posx + X_HP_BAR_OFFSET);
        enemyHPBar.setY((float) this.posy + Y_HP_BAR_OFFSET);
        // Increment traversed when we reach each block, so we can move to the next.
        if ( Math.abs(destX-this.posx)<=velocity && Math.abs(destY-this.posy)<=velocity ) {
            this.traversed += 1;
        }
    }

    public Runnable slowEnemy = new Runnable(){
            public void run(){
                try {
                    setVelocity(getVelocity() / 2);
                    Thread.sleep(3000);
                    setVelocity(getVelocity() * 2);
                } catch (Exception e) {
                }
            }
        };
}
