package edu.umb.cs443.finals;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.media.Image;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TowerUIFragment extends Fragment {
    final static String ARG_TOWERTYPE = "towertype";
    final static String ARG_POSITION = "position";
    final static String ARG_LEVEL = "level";
    final static String ARG_DAMAGE = "damage";
    final static String ARG_COST = "cost";
    private String towerType;
    private int position, level, damage, cost;
    AnimationDrawable tower1Animation;
    private ImageView towerImageIcon;
    private TextView towerLevel, towerDamage, towerCost;
    private Button upgradeButton, sellButton;
    private OnFragmentInteractionListener mListener;


    public TowerUIFragment() {}

    public static TowerUIFragment newInstance(String towerType, int position) {
        TowerUIFragment fragment = new TowerUIFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TOWERTYPE, towerType);
        bundle.putInt(ARG_POSITION, position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            towerType = bundle.getString(ARG_TOWERTYPE);
            position = bundle.getInt(ARG_POSITION);
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tower_ui_view, container, false);

        towerImageIcon = view.findViewById(R.id.tower_image_icon);
        towerDamage = view.findViewById(R.id.tower_damage);
        towerLevel = view.findViewById(R.id.tower_level);
        towerCost = view.findViewById(R.id.tower_next_cost);
        upgradeButton = view.findViewById(R.id.button_upgrade);
        sellButton = view.findViewById(R.id.button_sell);
        TextView levelID = view.findViewById(R.id.LevelID);
        TextView damageID = view.findViewById(R.id.DamageID);


        // Set listeners when button clicked
        upgradeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int sendValue = position;
                sendBackPosition("Upgrade",sendValue);
            }
        });
        sellButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int sendValue = position;
                sendBackPosition("Sell",sendValue);
            }
        });
        getTowerStats(towerType);
        towerDamage.setText(String.valueOf(this.damage));
        towerLevel.setText(String.valueOf(this.level));
        towerCost.setText(String.valueOf(this.cost));

        // Set the text when empty land.
        if (towerType.equals("X")) {
            upgradeButton.setText("Buy");
            sellButton.setEnabled(false);
            levelID.setVisibility(View.INVISIBLE);
            towerLevel.setVisibility(View.INVISIBLE);
            damageID.setVisibility(View.INVISIBLE);
            towerDamage.setVisibility(View.INVISIBLE);
        }
        else {
            upgradeButton.setText("Upgrade");
            sellButton.setEnabled(true);
            // Add tower animation
            towerImageIcon.setBackgroundResource(R.anim.tower1_animate);
            tower1Animation = (AnimationDrawable) towerImageIcon.getBackground();
            tower1Animation.start();
            levelID.setVisibility(View.VISIBLE);
            towerLevel.setVisibility(View.VISIBLE);
            damageID.setVisibility(View.VISIBLE);
            towerDamage.setVisibility(View.VISIBLE);
        }
        if (towerType.equals("X_3")) {
            upgradeButton.setEnabled(false);
            towerLevel.setText(String.valueOf(this.level) + " (MAX)");
            towerCost.setText("N/A");
        }
        else {
            upgradeButton.setEnabled(true);
        }

        return view;
    }

    public void sendBackPosition(String action,int sendValue) {
        if (mListener != null) {
            mListener.onFragmentInteraction(action,sendValue);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String action,int sendValue);
    }

    private void getTowerStats(String towerType) {
        if (towerType.equals("X")) {
            this.level = 0;
            this.damage = 0;
            this.cost = 10;
        } else if (towerType.equals("X_1")) {
            this.level = 1;
            this.damage = 5;
            this.cost = 20;
        } else if (towerType.equals("X_2")) {
            this.level = 2;
            this.damage = 15;
            this.cost = 40;
        } else if (towerType.equals("X_3")) {
            this.level = 3;
            this.damage = 45;
            this.cost = 80;
        }
        return;
    }
}