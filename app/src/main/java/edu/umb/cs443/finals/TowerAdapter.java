package edu.umb.cs443.finals;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class TowerAdapter extends BaseAdapter {
    private Context mContext;
    private int resourceLayout;
    AnimationDrawable grassAnimation;
    static String[] map;
    public Integer[] mTowerTypes = {
            R.drawable.grass_block_0, R.drawable.tower_2,
            R.drawable.tower_1, R.drawable.tower_0,
    };

    // Constructor
    public TowerAdapter(Context mContext, int resource, String[] map) {
        this.mContext = mContext;
        this.resourceLayout = resource;
        this.map = map;
    }

    // Create a new ImageView for each item referenced by the Adapter
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ResourceType")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView,towerImageView = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.tower, parent, false);
            imageView = convertView.findViewById(R.id.grid_item_base_image);
            towerImageView = convertView.findViewById(R.id.grid_item_tower_image);
            imageView.setLayoutParams(new GridView.LayoutParams(160, 170));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            towerImageView.setLayoutParams(new GridView.LayoutParams(160, 170));
            towerImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else { imageView = (ImageView) convertView;}

        // Set the image
        if (map[position].equals(" ")) {
            return imageView;
        } else if (map[position].equals("X")) {
            // Remove if there is a tower
            imageView.setImageDrawable(null);
            imageView.setBackgroundResource(R.anim.grass_animate);
            grassAnimation = (AnimationDrawable) imageView.getBackground();
            grassAnimation.start();
        } else if (map[position].equals("X_1")) {
            imageView.setImageResource(R.drawable.tower_1);
            imageView.setPadding(0,0,0,120);
        }else if (map[position].equals("X_2")) {
            imageView.setImageResource(R.drawable.tower_upgrade_2);
        } else if (map[position].equals("X_3")) {
            imageView.setImageResource(R.drawable.tower_upgrade_3);
        }
        return imageView;
    }


    public long getItemId(int position) {
        return position;
    }
    public Object getItem(int position) {
        return position;
    }
    public int getCount() {
        return this.map.length;
    }
}
