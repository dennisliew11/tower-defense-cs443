package edu.umb.cs443.finals;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class MainActivity extends FragmentActivity implements TowerUIFragment.OnFragmentInteractionListener{
    // UI
    private FrameLayout fragmentContainer;
    private MediaPlayer mediaPlayer;
    private GridView gridView;
    private TextView goldText, hpText;
    private ImageView goldUI, hpUI;
    private AnimationDrawable coinAnimation, heartAnimation;
    // Threads
    private Handler spawnHandler, enemyMoveHandler, killHandler, attackHandler;
    // Game variables
    private final static int INCREASE_DIFFICULTY = 5;
    private static final int SPAWN_DELAY = 6000;
    private RelativeLayout mainLayout;
    private int spawnX, spawnY;
    private boolean towerUIOpen = false;
    private int gold = 10;
    private int hp = 3;
    private int killed = 0;
    private Random r = new Random();
    private ArrayList<Enemy> enemies = new ArrayList<>();
    // Map variables
    private int spawnLocation;
    private ArrayList<Integer> walkPath;
    private Tower[] towers;
    private String[] map = null;
    static String[][] maplist = {
            {
                    "X", "X", " ", "X", "X", "X", "X",
                    "X", "X", " ", " ", "X", "X", "X",
                    "X", "X", "X", " ", " ", "X", "X",
                    "X", "X", "X", "X", " ", "X", "X",
                    "X", "X", " ", " ", " ", "X", "X",
                    "X", "X", " ", "X", "X", "X", "X",
                    "X", "X", " ", "X", "X", "X", "X",
                    "X", "X", " ", "X", "X", "X", "X",
            },
            {
                    "X", "X", "X", "X", "X", " ", "X",
                    "X", " ", " ", " ", " ", " ", "X",
                    "X", " ", "X", "X", "X", "X", "X",
                    "X", " ", " ", " ", " ", " ", "X",
                    "X", "X", "X", "X", "X", " ", "X",
                    "X", " ", " ", " ", " ", " ", "X",
                    "X", " ", "X", "X", "X", "X", "X",
                    "X", " ", "X", "X", "X", "X", "X",
            },
            {
                    "X", "X", "X", "X", "X", " ", "X",
                    "X", "X", "X", "X", " ", " ", "X",
                    "X", "X", "X", " ", " ", "X", "X",
                    "X", "X", " ", " ", "X", "X", "X",
                    "X", " ", " ", "X", "X", "X", "X",
                    "X", " ", "X", "X", "X", "X", "X",
                    "X", " ", " ", " ", " ", " ", "X",
                    "X", "X", "X", "X", "X", " ", "X",
            },
        };

    @SuppressLint("ResourceType")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Randomize a map layout from above
        this.map = maplist[r.nextInt(maplist.length)];
        this.towers = new Tower[this.map.length];
        findEnemySpawnLocation(map);
        findWalkPath();
        setContentView(R.layout.activity_main);
        mainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        // UI stuff
        fragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);
        gridView = (GridView) findViewById(R.id.gridView1);
        goldText = (TextView) findViewById(R.id.goldTextCell);
        goldUI = (ImageView) findViewById(R.id.top_ui_coin);
        hpUI = (ImageView) findViewById(R.id.top_ui_heart);
        hpText = (TextView) findViewById(R.id.hpTextCell);
        goldUI.setBackgroundResource(R.anim.coin_animate);
        hpUI.setBackgroundResource(R.anim.heart_animate);
        coinAnimation = (AnimationDrawable) goldUI.getBackground();
        heartAnimation = (AnimationDrawable) hpUI.getBackground();
        // Grid Map
        gridView.setAdapter(new TowerAdapter(this,R.layout.tower,map));
        init();
        // Background update gold/hp UI
        new PerformAsyncUIUpdate().execute();
        // Listen for player placement click
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
//                Toast.makeText(getApplicationContext(),
//                        (CharSequence) (
//                                ),
//                        Toast.LENGTH_LONG).show();
                openUIFragment(true,position);
            }
        });
        // -- THREADS --
        // Background spawn enemies
        spawnHandler = new Handler();
        spawnHandler.post(handleSpawn);
        // Background move enemies
        enemyMoveHandler = new Handler();
        enemyMoveHandler.post(handleMovement);
        // Background remove dead enemies
        killHandler = new Handler();
        killHandler.post(handleRemoveEnemy);
        // Background tower attack handler
        attackHandler = new Handler();
        attackHandler.post(handleAttackEnemy);
    }

    @SuppressLint("ResourceType")
    private void playUpgradeAnimation(boolean first, int location) {
        // imageView settings
        LayoutInflater layoutInflater;
        layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ImageView towerImageView = (ImageView) layoutInflater.inflate(R.layout.enemy_image,
                null);
        towerImageView.setImageDrawable(null);
        if (first) {
            towerImageView.setBackgroundResource(R.anim.tower_1_upgrade_2_animate);
        } else {
            towerImageView.setBackgroundResource(R.anim.tower_2_upgrade_3_animate);
        }
        View v1 = gridView.getChildAt(location);
        if (v1 != null) {
            towerImageView.setX(v1.getX() + v1.getWidth() / 2 - 80);
            towerImageView.setY(v1.getY() + v1.getHeight() / 2 - 15);
        }
        towerImageView.setLayoutParams(new GridView.LayoutParams(160, 170));
        towerImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        AnimationDrawable towerAnimation = (AnimationDrawable) towerImageView.getBackground();
        towerAnimation.start();
        // --
        mainLayout.addView(towerImageView, 1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mainLayout.removeView(towerImageView);
                ((TowerAdapter) gridView.getAdapter()).notifyDataSetChanged();
            }
        }, 400);
    }

    @Override
    public void onFragmentInteraction(String action,int mapPosition) {
        String mapValue = map[mapPosition];
        // Button Upgrade pressed and Create new tower at that position
        if (action.equals("Upgrade")) {
            if (canBuy(mapValue,mapPosition)) {
                playSoundFile(R.raw.level,0.6f);
                if (mapValue == "X") {
                    map[mapPosition] = "X_1";
                    this.towers[mapPosition] = new Tower(map[mapPosition], mapPosition);
                    ((TowerAdapter) gridView.getAdapter()).notifyDataSetChanged();
                } else if (mapValue == "X_1") {
                    map[mapPosition] = "X_2";
                    playUpgradeAnimation(true,mapPosition);
                    this.towers[mapPosition] = new Tower(map[mapPosition], mapPosition);
                } else if (mapValue == "X_2") {
                    map[mapPosition] = "X_3";
                    playUpgradeAnimation(false,mapPosition);
                    this.towers[mapPosition] = new Tower(map[mapPosition], mapPosition);
                }
            }
        }
        // Button Sell pressed
        else if (action.equals("Sell")) {
            if (mapValue.startsWith("X_")) {
                map[mapPosition] = "X";
                // Return some gold to player when selling towers (half the cost)
                this.gold += (int) Math.floor( this.towers[mapPosition].getCost() / 4 );
                this.towers[mapPosition] = null;
                ((TowerAdapter) gridView.getAdapter()).notifyDataSetChanged();
            }
        }
        openUIFragment(false,mapPosition);
    }

    private boolean canBuy(String towerType, int pos) {
        int deduction = Integer.MAX_VALUE;;
        if (towerType=="X") {deduction = 10;}
        else if (towerType=="X_1") {deduction = towers[pos].getCost();}
        else if (towerType=="X_2") {deduction = towers[pos].getCost();}
        if (this.gold >= deduction) {
            this.gold -= deduction;
            return true;
        } else {
            return false;
        }
    }

    private void findEnemySpawnLocation(String[] map) {
        int n = 7;
        // Get only first row
        for(int i = 0; i< n; i++)
            if (map[i]==" " && map[i+1]!=" ") {
                this.spawnLocation =  i;
                return;
            }
        this.spawnLocation =  -1;
            return;
    }

    //play a soundfile once
    public void playSoundFile(Integer fileName, float volume){
        mediaPlayer = MediaPlayer.create(this, fileName);
        mediaPlayer.setVolume(volume,volume);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            };
        });
    }

    //play a soundfile looped
    public void playSoundFileLoop(Integer fileName, float volume){
        mediaPlayer = MediaPlayer.create(this, fileName);
        mediaPlayer.setVolume(volume,volume);
        mediaPlayer.start();
        mediaPlayer.setLooping(true);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            };
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        coinAnimation.start();
        heartAnimation.start();
        playSoundFileLoop(R.raw.happy,0.5f);
    }

    void init(){
        goldText.setText(Integer.toString(gold));
        hpText.setText(Integer.toString(hp));
        ((TowerAdapter)gridView.getAdapter()).notifyDataSetChanged();
    }

    // Spawns 1 enemy object
    @SuppressLint("ResourceType")
    private void spawnEnemy() {
        int difficulty = (int) Math.floor(killed / INCREASE_DIFFICULTY);
        LayoutInflater layoutInflater;
        layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // Handle Monster Spawner thread
        View v1 = gridView.getChildAt(this.spawnLocation);
        if (v1 != null) {
            this.spawnX = (int) v1.getX();
            this.spawnY = (int) v1.getY() + v1.getHeight() / 2;
        }
        //enemy init
        int proportionalVelocity = 10;
        Enemy mEnemy = new Enemy(this.spawnX,this.spawnY,difficulty, this.map);
        this.enemies.add(mEnemy);
        mEnemy.setVelocity(proportionalVelocity);
        // enemy imageView settings
        ImageView enemyImageView = (ImageView) layoutInflater.inflate(R.layout.enemy_image, null);
        mEnemy.setEnemyImageView(enemyImageView);
        ProgressBar enemyHPBar = (ProgressBar) layoutInflater.inflate(R.layout.enemy_hp_bar
                , null);
        mEnemy.setEnemyHPBar(enemyHPBar);
        // --
        mainLayout.addView(enemyImageView, 1);
        mainLayout.addView(enemyHPBar, 1);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void spawnProjectile(float sourceX, float sourceY, float destX, float destY, Tower tower,
                                 Enemy enemy) {

        double distance = Math.sqrt( Math.pow(sourceX-destX,2) + Math.pow(sourceY-destY,2) );

        final ImageView projectileView = new ImageView(getApplicationContext());
        switch (tower.getLevel()) {
            case 1:
                projectileView.setImageResource(R.drawable.tower_projectile_1);
                break;
            case 2:
                projectileView.setImageResource(R.drawable.tower_projectile_2);
                break;
            case 3:
                projectileView.setImageResource(R.drawable.tower_projectile_3);
                break;
        }

        projectileView.setLayoutParams(new GridView.LayoutParams(50, 50));
        projectileView.setX(sourceX);
        projectileView.setY(sourceY);
        mainLayout.addView(projectileView);
        // Path properties
        Path path = new Path();
        path.moveTo(sourceX,sourceY);
        path.lineTo(destX,destY);
        ObjectAnimator animator = ObjectAnimator.ofFloat(projectileView, "translationX",
                "translationY", path);
        int DURATION = 90;
        animator.setDuration(DURATION);
        animator.start();
        //----
        enemy.hit(tower.getDamage());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mainLayout.removeView(projectileView);
            }
        }, DURATION);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void spawnCoin(Enemy enemy) {
        float x = (float) (enemy.getPosX() + enemy.getEnemyImageView().getWidth() / 2 - 25);
        float y = (float) (enemy.getPosY() + enemy.getEnemyImageView().getHeight() / 2 - 10);
        float destX = (float) goldUI.getX();
        float destY = (float) goldUI.getY();

        final ImageView droppedCoinView = new ImageView(getApplicationContext());
        final int reward = enemy.getReward();
        droppedCoinView.setImageResource(R.drawable.faceon_gold_coin);
        droppedCoinView.setLayoutParams(new GridView.LayoutParams(40, 40));
        droppedCoinView.setX((float) x);
        droppedCoinView.setY((float) y);
        mainLayout.addView(droppedCoinView);
        Path path = new Path();
        path.moveTo(x,y);
        path.lineTo(x,y-30);
        path.lineTo(x,y);
        path.lineTo(x,y);
        path.lineTo(x,y);
        path.lineTo(x,y);
        path.lineTo(x,y);
        path.lineTo(x,y);
        path.lineTo(destX,destY);
        ObjectAnimator animator = ObjectAnimator.ofFloat(droppedCoinView, "translationX",
                "translationY", path);
        int DURATION = 1000;
        animator.setDuration(DURATION);
        animator.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mainLayout.removeView(droppedCoinView);
                gold += reward;
            }
        }, DURATION);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void attackEnemy() {
        int x1,x2,y1,y2;

        for (int i=0; i < towers.length; i++) {
            if (towers[i]!=null) {
                for (Enemy enem : enemies) {
                    // Tower coordinates
                    View v1 = gridView.getChildAt(i);
                    x1 = (int) v1.getX() + v1.getWidth() / 2 - 25;
                    y1 = (int) v1.getY() + v1.getHeight() / 2;
                    // Enemy coordinates
                    x2 = enem.getPosX() + enem.getEnemyImageView().getWidth() / 2 - 25;
                    y2 = enem.getPosY() + enem.getEnemyImageView().getHeight() / 2 - 10;
                    // If tower's distance is less than range limit,
                    // ATTACK the enemy with tower damage.
                    double distance = Math.sqrt( Math.pow(x1-x2,2) + Math.pow(y1-y2,2) );
                    if (distance <= towers[i].getRange()) {
                        spawnProjectile((float) x1, (float) y1, (float) x2, (float) y2, towers[i], enem);
                        break;
                    }
                }
            }
        }
    }

    public void openUIFragment(boolean interact, int position) {
        if (towers[position] != null) {
        }
        //
        if (map[position].equals(" ")) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStackImmediate();
        } else {
            TowerUIFragment fragment = TowerUIFragment.newInstance(map[position],position);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (interact) {
                transaction.setCustomAnimations(R.anim.ui_actions_open, R.anim.ui_actions_close);
            }
            fragmentManager.popBackStackImmediate();
            transaction.addToBackStack(null);
            transaction.replace(R.id.fragment_container, fragment,"TOWER_FRAGMENT").commit();
        }
    }

    private void findWalkPath() {
        this.walkPath = new ArrayList<Integer>();
        int cur_idx = this.spawnLocation;
        int nCols = 7;
        int nRows = map.length / nCols;

        while (cur_idx >= 0 && cur_idx < this.map.length - nCols) {
            int down_idx = cur_idx + nCols;
            int left_idx = cur_idx - 1;
            int right_idx = cur_idx + 1;
            // cur_idx not at last row?
            if (Math.floor(cur_idx / nCols) < nRows - 1 && map[down_idx].equals(" ")) {
                cur_idx = down_idx;
                this.walkPath.add(cur_idx);
            } // cur_idx not at leftmost row?
            else if (cur_idx % (nCols) != 0 && map[left_idx].equals(" ") && !this.walkPath.contains(left_idx)) {
                cur_idx = left_idx;
                this.walkPath.add(cur_idx);
            } // cur_idx not at rightmost row?
            else if (cur_idx % (nCols) != nCols - 1 && map[right_idx].equals(" ") && !this.walkPath.contains(right_idx)) {
                cur_idx = right_idx;
                this.walkPath.add(cur_idx);
            } else {
                return;
            }
        }
        return;
    }

    public void updateUI() {
        this.goldText.setText(String.valueOf(this.gold));
        this.hpText.setText(String.valueOf(this.hp));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        finish();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        finish();
        super.onDestroy();
    }

    // Thread to periodically spawns an enemy
    public Runnable handleSpawn = new Runnable() {
        @Override
        public void run() {
            try {
                spawnHandler.postDelayed(this, SPAWN_DELAY);
                // Only start this when the gridView has finished loading its last element.
                // because we need to find starting position with findEnemySpawnLocation().
                if (gridView.getChildAt(map.length-1) != null) {
                    spawnEnemy();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    // Thread to periodically attack an enemy
    public Runnable handleAttackEnemy = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void run() {
            try {
                spawnHandler.postDelayed(this, 1000);
                // Only start this when the gridView has finished loading its last element.
                // because we need to find starting position with findEnemySpawnLocation().
                if (gridView.getChildAt(map.length-1) != null) {
                    attackEnemy();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    // Thread to periodically remove dead enemy
    public Runnable handleRemoveEnemy = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        private void doRemove(Enemy enem) {
            // Remove sprite and hp bar
            enem.getEnemyImageView().setImageDrawable(null);
            enem.getEnemyImageView().clearAnimation();
            mainLayout.removeView(enem.getEnemyHPBar());
            mainLayout.removeView(enem.getEnemyImageView());
            playSoundFile(R.raw.coin_collect,0.6f);
            enemies.remove(enem);
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void run() {
            try {
                spawnHandler.postDelayed(this, 100);
                for (Enemy enem : enemies)
                {
                    // Enemy reached the END!
                    if (enem.getPrepareKill() && enem.getTraversed()==walkPath.size()) {
                        doRemove(enem);
                        hp -= 1;
                    }
                    // Enemy killed by tower
                    if (enem.getLife() <= 0) {
                        doRemove(enem);
                        killed += 1;
                        spawnCoin(enem);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    // Thread to periodically move all enemies
    public Runnable handleMovement = new Runnable() {
        @Override
        public void run() {
            try {
                enemyMoveHandler.postDelayed(this, 300);
                // Only start this when the gridView has finished loading its last element.
                // because we need to find starting position with findEnemySpawnLocation().
                if (gridView.getChildAt(map.length-1) != null) {
                    for (Enemy enem : enemies)
                    {
                        int traversed = enem.getTraversed();
                        View v1 = gridView.getChildAt(walkPath.get(traversed));
                        int newX = (int) v1.getX();
                        int newY = (int) v1.getY() + v1.getHeight() / 2;
                        enem.move(newX,newY);
                        if (traversed==walkPath.size()-1) {
                            // remove this enemy
                            enem.reachedEnd();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    // Gold and HP UI handler
    private class PerformAsyncUIUpdate extends AsyncTask<Void, Integer, Void> {

        @Override
        protected void onPreExecute() {
            // UPDATE THE UI IMMEDIATELY BEFORE BACKGROUND WORK IS PERFORMED
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            runOnUiThread(new Runnable() {
                              @Override
                              public void run() {}
                          }
            );
            while (true) {
                try {
                    publishProgress();
                    Thread.sleep(100);
                } catch (Exception e) {
                }
            }
        }
        // Update the UI every 0.1s
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            updateUI();
            if(hp <= 0) {
                Toast.makeText(getApplicationContext(),
                        (CharSequence) ("Lost all lives!"),
                        Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    };
}
