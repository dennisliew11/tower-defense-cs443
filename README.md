
<table cellspacing="0" cellpadding="0" align="center"><tr><td align="center" width="9999">
<img src="/report/icon.png" align="center" width="150" alt="Project icon">

<h1 >
Tower Defense Game
</h1>

_**CS443 Spring 2021 Final Project**_ *by Dennis Liew*
</td></tr></table>

This README includes a report of the game development, structured in the following:

1. [Game Overview](#1-game-overview)
2. [Map](#2-map)
3. [Towers](#3-towers)
4. [Enemy](#4-enemy)
5. [Demo](#5-demo)

# 1. Game Overview


The game is a Tower Defense (TD), where the player places towers to prevent monsters from reaching the exit located at the bottom. The goal is to survive as long as possible, as the difficulty of the monsters increases. Players will gain gold from killing monsters to buy or upgrade more towers


<div class="row">
  <div class="column">
    <img src="report/ui_top.png" alt="map1" width="22%">
  </div>
</div>

The UI on the top of the screen shows the player gold <img src="app/src/main/res/drawable/faceon_gold_coin.png" width="15" height="15"> and HP <img src="app/src/main/res/drawable/face_on_heart.png" width="15" height="15">. When HP reaches 0, the player loses.

# 2. Map

There are three distinct 7 X 8 grid map layouts. These are randomly selected when the game starts, offering a variety of placement options.
    <img src="report/map1.png" alt="map1" width="20%">
    <img src="report/map2.png" alt="map2" width="20%">
    <img src="report/map3.png" alt="map3" width="20%">

The maps also come with a path where the enemies will take. Enemies enter from the top and exit at the bottom.
These maps are generated from a 56 length String array, where "X" represents a build-able grass spot and " " represents a monster path (non-build-able spot).


An example for the right image in String array:
```java
static String[][] maplist = {
{
        "X", "X", " ", "X", "X", "X", "X",
        "X", "X", " ", " ", "X", "X", "X",
        "X", "X", "X", " ", " ", "X", "X",
        "X", "X", "X", "X", " ", "X", "X",
        "X", "X", " ", " ", " ", "X", "X",
        "X", "X", " ", "X", "X", "X", "X",
        "X", "X", " ", "X", "X", "X", "X",
        "X", "X", " ", "X", "X", "X", "X",
},
```
The [findEnemySpawnLocation()](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/MainActivity.java#L230) function locates the starting position to spawn the enemy. Each grid is made of a [towerAdapter Object](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/TowerAdapter.java) which determines if the player can interact with the sub-grid. Grass tiles are drawn on spots that are marked "X".

<img src="report/grass.gif" alt="map1" width="30">

Interactable grass sub-grids will send a [callback](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/TowerUIFragment.java#L73) to main-activity when clicked, which spawns a [towerUI Fragment](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/TowerUIFragment.java) as seen below.

<img src="report/ui_1.png" width="20%">
<img src="report/ui_2.png" width="20%">

(Image left) is when the monster path is clicked. (Image right) is when the grass spots is clicked. These are build-able spots.


# 3. Towers
Towers are player placed objects on the grass tiles. They attack the enemy within their attack range. They cost gold to buy and upgrade, and can also be sold for 50% cost refund. There are 3 levels of tower upgrades:

<img src="report/grass.gif" alt="map1" width="20">
⮕
<img src="report/tower_1.gif" alt="map1" style="width:11%;padding: 5px;">
⮕
<img src="/app/src/main/res/drawable/tower_upgrade_2.png" alt="map1" style="width:11%;padding: 5px;">
⮕
<img src="/app/src/main/res/drawable/tower_upgrade_3.png" alt="map1" style="width:11%;padding: 5px;">

The towers also change their attack projectile image depending on level. <img src="/app/src/main/res/drawable/tower_projectile_1.png" alt="map1" style="width:3%;padding: 5px;"><img src="/app/src/main/res/drawable/tower_projectile_2.png" alt="map1" style="width:3%;padding: 5px;"><img src="/app/src/main/res/drawable/tower_projectile_3.png" alt="map1" style="width:3%;padding: 5px;">

The [tower Object](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/Tower.java) specifies the attributes of the towers. These are tower type, grid position, upgrade level, attack damage, gold cost, and attack range. Tower attacks are handled by a [attackHandler](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/MainActivity.java#L504) to periodically attack **one** enemy within range.

Below is the [increment level function](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/Tower.java#L33) which determines the attributes of the next level of towers at each upgrade invoked.

```java
private void incrLevel() {
        this.level += 1;
        this.cost *= 2;
        this.damage *= 3;
        this.range += 80f;
    }
```
I've built custom animations when the towers go through an upgrade. Handled [here](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/MainActivity.java#L150).

<img src="/report/upgrade_1.gif" alt="map1" style="width:11%;padding: 5px;">
<img src="/report/upgrade_2.gif" alt="map1" style="width:11%;padding: 5px;">

When the player clicks on an existing tower. Its attributes represented by the level will be displayed on the UI. This is controlled by the [onFragmentInteraction()](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/MainActivity.java#L183) function that codes for the buy, upgrade and sell UI interactions. It checks for cost and updates the player gold accordingly.

**NOTE:** I've forgotten to update each UI's tower image corresponding to upgrade level.  They all show level 1 upgrade image.

<img src="report/ui_lvl1.png" alt="map1" width="20%">
<img src="report/ui_lvl2.png" alt="map2" width="20%">
<img src="report/ui_lvl3.png" alt="map2" width="20%">

# 4. Enemy

The enemy sprites:
<img src="/report/zombie.gif" alt="map1" width="6%">

These enemies spawn at the top of the path, also created periodically with a [spawnHandler](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/MainActivity.java#L487) set to spawn every 6 seconds. Their attributes are specified in the [enemy Object](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/Enemy.java). Notable attributes are HP, gold reward, and velocity.

Their movements are controlled by the [move()](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/Enemy.java#L88) function, path taken is calculated in the [findWalkPath()](https://gitlab.com/dennisliew11/tower-defense-cs443/-/blob/master/app/src/main/java/edu/umb/cs443/finals/MainActivity.java#L435) function. These help the enemy generate a walking path even on different map layouts, and also prevent cycles.

The enemy difficulty is incremented after a certain **n** enemies are killed. Then the next spawned enemy's HP is increased. Of course, their rewards are also increased if killed.

There are static variables in main activity which control **n** and spawn timer:

```java
private final static int INCREASE_DIFFICULTY = 5;
private static final int SPAWN_DELAY = 6000;
```

Below is the code that creates the next spawned Enemy with increased difficulty:

```java
public Enemy(int posx, int posy, int difficulty, String[] map) {
        this.posx = posx;
        this.posy = posy;
        this.difficulty = difficulty;
        this.max_life = 7 + this.difficulty * 20;
        this.life = (int) this.max_life;
        this.reward = 1 + this.difficulty;
        this.map = map;
        this.traversed = 0;
        this.prepareKill = false;
    }
```

When the enemy reaches the end of the path, the deduct 1 HP from the player. If they are killed by the towers, they drop a gold reward for the player.

# 5. Demo
![](/report/demo.mp4)
